import React, { useState, useEffect } from "react";
import Grid from "@material-ui/core/Grid";
import { Typography, Button } from "@material-ui/core";
import line from "../../images/hj-line.png";
import ArrowBackIos from "@material-ui/icons/ArrowBackIos";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import PrnTable from "../../components/TableComponent/TableComponent";
import useStyles from "./styles.js";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TableDropdown from "../../components/TableComponent/TableDropdown";
import TextField from "@material-ui/core/TextField";
import Snackbar from "@material-ui/core/Snackbar";

import { ViewPRN, PRNGeneration } from "../../config/inventoryAPIs";

import { isEmpty } from "../../HelperFunctions/help";
import CircularProgress from "@material-ui/core/CircularProgress";



function PrnDetails(props) {
const classes = useStyles();
const handleClick = e => {
e.preventDefault();
console.log(props);
props.history.push({
pathname: "/Inventory"
});
};

let RowDetails = [];
let [data, setdata] = useState();
let [loader, setloader] = useState(false);

let [suppliers, setSuppliers] = useState([
{
supplierId: 1,
supplierName: "EXPIRED"
} / ,{"supplierId":2,"supplierName":"Damaged"} /
]);

let Gdate = new Date();

const [state, setState] = useState({
open: false,
vertical: "top",
horizontal: "center",
message: ""
});

const { vertical, horizontal, open, message } = state;

function handleClose() {
setState({ ...state, open: false });
}

const columns = [
{
name: "skuName",
label: "PRN Item",
options: {
filter: true
}
},
{
name: "expectedQty",
label: "Qty",
options: {
filter: true
}
},
{
name: "rejectedQty",
label: "Rejected Qty",
options: {
filter: true,
customBodyRender: (value, tableMeta, updateValue) => (
<FormControlLabel
control={<TextField value={value || ""} type="number" />}
onChange={event => {
updateValue(event.target.value);
data[tableMeta.rowIndex].rejectedQty = +event.target.value;
data[tableMeta.rowIndex].grnRejectedQty = +event.target.value;
}}
/>
)
}
},
{
name: "rejectReason",
label: "Reject Reason",
options: {
filter: false,
customBodyRender: (value, tableMeta, updateValue) => {
return (
<TableDropdown
value={value}
index={tableMeta.columnIndex}
change={event => {
updateValue(event);
data[tableMeta.rowIndex].skuPrnRejectionReason = event;
}}
cities={suppliers}
/* onClick= {SupplierDropdown}*/
/>
);
}
}
},
{
name: "batchNo",
label: "Batch No",
options: {
filter: false,
customBodyRender: (value, tableMeta, updateValue) => (
<FormControlLabel
control={<TextField value={value || ""} type="text" />}
onChange={event => updateValue(event.target.value)}
/>
)
}
},
{
name: "mfgDate",
name: "Mfg. Date",
options: {
filter: false,
customBodyRender: (value, tableMeta, updateValue) => (
<FormControlLabel
control={<TextField value={value || ""} type="date" />}
onChange={event => {
updateValue(event.target.value);
data[tableMeta.rowIndex].mfgDate = event.target.value + " 00:00";
}}
/>
)
}
}
];

console.log("PRN Details", props.location.state.detail);

useEffect(() => {
setloader(true);
ViewPRN.post("/", {
peNo: `${props.location.state.detail[0]}`,
invoiceId: +`${props.location.state.detail[6]}`
})
.then(res => {
console.log("PRN details res", res);
return res.data.prnProductsDetails;
})
.then(FinalJson => {
setdata(FinalJson);
setloader(false);
console.log("final PRN details", FinalJson);
})
.catch(error => {
console.log("error data", error);
});
}, []);

const PRNSubmit = () => {
console.log("data", data);

setloader(true);

PRNGeneration.post(
"/",

{
invoiceId: props.location.state.detail[6],
prnProductDetails: data,
purEstimateNo: props.location.state.detail[0],
supplierName: props.location.state.detail[3]
}
)
.then(res => {
console.log("PRN details,", res);

setState({
open: true,
vertical: "top",
horizontal: "center",
message: `sucessfully generated PRN & ${res.data.prnNo}`
});

setdata(data);
setloader(false);
})
.catch(error => {
//console.log("error---------",error.response.data.errorMessage);
setState({
open: true,
vertical: "top",
horizontal: "center",
message: `Facing some issues. Please try again!`
});

setdata(data);
setloader(false);
});
};

function getValueBack(selectedRow) {
console.log("getValueBack", selectedRow);

if (!isEmpty(selectedRow[0])) {
RowDetails = selectedRow;
} else {
RowDetails = [];
}
}

let cdate;
function handleDateChange(date) {
cdate = date;
}

return (
<React.Fragment>
<Grid container className={classes.topGrid} />
<Grid container className={classes.VHeader}>
<Grid item xs="6">
<Breadcrumbs aria-label="Breadcrumb">
<Link onClick={handleClick} className={classes.link}>
<ArrowBackIos className={classes.icon} />
Back
</Link>
</Breadcrumbs>
<Typography className={classes.headingprd}>PRN Details</Typography>
<img src={line} className={classes.lineImg} alt="image" />
</Grid>
</Grid>
<Grid container className={classes.tableHeader}>
<Grid item xs="3">
<Typography className={classes.gridHeader}>PE Number</Typography>
<Typography className={classes.gridData}>
{props.location.state.detail[0]}{" "}
</Typography>
</Grid>
<Grid item xs="2">
<Typography className={classes.gridHeader}>PE Date</Typography>
<Typography className={classes.gridData}>
{props.location.state.detail[1]}
</Typography>
</Grid>
<Grid item xs="2">
<Typography className={classes.gridHeader}>Vendor Name</Typography>
<Typography className={classes.gridData}>
{props.location.state.detail[3]}
</Typography>
</Grid>
<Grid item xs="5" className={classes.buttonGrid}>
<Button
variant="contained"
className={classes.buttonStyle}
onClick={PRNSubmit}
>
Submit
</Button>
</Grid>
</Grid>

<Snackbar
anchorOrigin={{ vertical, horizontal }}
key={`${vertical},${horizontal}`}
open={open}
onClose={handleClose}
ContentProps={{
"aria-describedby": "message-id",
classes: {
root: classes.snackbarbg
}
}}
message={message}
/>
<Grid container style={{ padding: "3%" }}>
{loader == true ? (
<CircularProgress className={classes.spinner} disableShrink />
) : (
<PrnTable
{...props}
columns={columns}
data={data}
getValue={getValueBack}
selectableRows="none"
pagination={false}
/>
)}
</Grid>
</React.Fragment>
);
}

export default PrnDetails;