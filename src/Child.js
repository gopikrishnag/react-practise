import React from "react";
import MUIDataTable from "mui-datatables";
import Grid from "@material-ui/core/Grid";

import { makeStyles, useTheme } from "@material-ui/core/styles";
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';

import {SuppliersList} from "../../config/inventoryAPIs";

const useStyles = makeStyles(theme => ({
Datatable: {
width: "100%",
},
}));
export default function TableComponent(props) {

const getMuiTheme = () => createMuiTheme({
overrides: {
MUIDataTableToolbarSelect:{
root:{
display:"none"
}
},
MUIDataTableBodyCell:{
root:{
width:"200px",
height:"50px",
padding: "0px 15px 0px 15px",
}
},
MuiSvgIcon:{
root:{
width: "0.75em",
width: "0.75em",
}
},
}

})

//console.log("tableeee",props)
const classes = useStyles();


const path = props.path;
const options = {
responsive: props.responsive,
filterList: false,
sortFilterList: false,
filter: false,
search: false,
print: false,
download: false,
checkbox: false,
viewColumns:false,
selectableRows: props.selectableRows,
pagination:props.pagination,
// download	:props.download,
onRowClick: function handleRowClick(e) {
console.log("Row Clicked data", path, props,e);

if (path) { 
props.history.push({
pathname: path,
state: { detail: e }
}); 

}

} , 
onTableChange: (action, dataObj) => {
console.log("onTableChange")
let actualData = [];
if (dataObj.selectedRows.data.length > 0) {
console.log("Row Selected");
var selectedRowIndices = Object.keys(dataObj.selectedRows.lookup);
selectedRowIndices.map(value => {
actualData.push(dataObj.data[value].data);
});
console.log("dataaaaa",actualData)
if(props.getValue){
props.getValue(actualData);
}

} else {
console.log("No rows selected");
if(props.getValue){
props.getValue([]); 
// props.getValue([{}]);
}
}
},

};

return (
<Grid container>
<MuiThemeProvider theme={getMuiTheme()}> 
<MUIDataTable
className={classes.Datatable}
title={props.title}
data={props.data}
columns={props.columns}
options={options}
/>
</MuiThemeProvider>
</Grid>
);
}