import React from 'react';
import './counter.css';

class Counter extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      counter : props.counter
  }
    //To avoid bindings the methods from constructor, Use the arrow functions
   // this.increment = this.increment.bind(this);
    //this.reset = this.reset.bind(this);
  }
    
    render()  {
      return(
        <div id="root">
          <button className="clickMe"  onClick={this.increment}>click me (+{this.props.incrementer}) </button>
          <div>
            Current clicks : {this.props.counter}
          </div>
          <div>
            <button onClick={this.reset} className="reset">Resest</button>
          </div>
        </div>
      );
    }

    increment = () =>    {
      this.props.count(this.props.incrementer);
    }

    reset = ()  => {
      //this.props.reset();
    }
}

//Adding default values for propes, If not provided
Counter.defaultProps = {
  incrementer:1
}


export default Counter;
