import React from "react";
import "./counter.css";
import Counter from "./Counter";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      counter: 0
    };
  }


  incrementer = (inc) => {
    this.setState({
        counter: this.state.counter+inc
      })
  }

  reset = () => {
    this.setState({
      counter: 0
    })
  }

  

  render() {
    return (
      <div>
        {/* Default value for incrementer is was setted in Counter.js
            If no props set here that will get picks up. 
        */}
        <Counter incrementer={3} count={this.incrementer} counter={this.state.counter} />
        <Counter incrementer={2} count={this.incrementer} counter={this.state.counter} />
      </div>
    );
  }
}
export default App;
